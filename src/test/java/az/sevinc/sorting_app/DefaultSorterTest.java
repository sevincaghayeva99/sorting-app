package az.sevinc.sorting_app;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.runners.Parameterized.*;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class DefaultSorterTest {

    private String[] input;
    private int[] expectedOutput;

    public DefaultSorterTest(String[] input, int[] expectedOutput) {
        this.input = input;
        this.expectedOutput = expectedOutput;
    }

    @Parameters
    public static Collection<Object[]> testCases() {
        return Arrays.asList(new Object[][]{
                {new String[]{}, new int[]{}},                           // Empty array
                {new String[]{"5"}, new int[]{5}},                       // Single element
                {new String[]{"3", "1", "4", "2"}, new int[]{1, 2, 3, 4}}, // Unsorted array
                {new String[]{"-2", "0", "10", "-5"}, new int[]{-5, -2, 0, 10}}, // Negative numbers
                {new String[]{"1", "2", "3", "3", "2", "1"}, new int[]{1, 1, 2, 2, 3, 3}}, // Duplicates
        });
    }

    @Test
    public void testSort() {
        DefaultSorter sorter = new DefaultSorter();
        int[] result = sorter.sort(input);
        assertArrayEquals(expectedOutput, result);
    }

    @Test(expected = NumberFormatException.class)
    public void testSort_InvalidInput() {
        DefaultSorter sorter = new DefaultSorter();
        String[] invalidInput = {"1", "2", "3", "abc"}; // Invalid input: "abc"
        sorter.sort(invalidInput); // Should throw NumberFormatException
    }

    @Test
    public void testSort_NoArguments() {
        DefaultSorter sorter = new DefaultSorter();
        String[] noArguments = {};
        int[] result = sorter.sort(noArguments);
        assertEquals(0, result.length); // Should return an empty array
    }
}