package az.sevinc.sorting_app;

import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The DefaultSorter class provides sorting functionality for an array of integers.
 */
public class DefaultSorter {

    private static final Logger logger = LoggerFactory.getLogger(DefaultSorter.class);

    /**
     * Sorts the given array of strings representing integers in ascending order.
     * If no arguments are provided, an empty array is returned.
     * If any argument is not a valid integer, an exception is thrown.
     *
     * @param args The array of strings representing integers to be sorted.
     * @return An array of integers sorted in ascending order.
     * @throws NumberFormatException If any argument is not a valid integer.
     */
    public int[] sort(String[] args) {
        if (args.length == 0) {
            logger.info("No arguments provided.");
            return new int[] {};
        }

        int[] numbers = new int[args.length];
        for (int i = 0; i < args.length; i++) {
            try {
                numbers[i] = Integer.parseInt(args[i]);
            } catch (NumberFormatException e) {
                logger.error("Argument {} is not a valid integer.", args[i]);
                throw e;
            }
        }

        Arrays.sort(numbers);

        return numbers;
    }
}
