package az.sevinc.sorting_app;

import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Main class represents the entry point of the program.
 */
public class Main {

    private static final Logger logger = LoggerFactory.getLogger(Main.class);

    /**
     * The main method is the entry point of the program.
     *
     * @param args The command-line arguments passed to the program.
     */
    public static void main(String[] args) {
        DefaultSorter sorter = new DefaultSorter();

        String inputStr = Arrays.toString(args);

        logger.info("Given input: {}", inputStr);

        int[] result = sorter.sort(args);
        String resultStr = Arrays.toString(result);

        logger.info("Result: {}", resultStr);
    }
}